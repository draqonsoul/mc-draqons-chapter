package com.draqon.blocks;

import java.util.concurrent.atomic.AtomicInteger;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.draqon.setup.Registration;
import com.draqon.tools.CustomEnergyStorage;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.crafting.RecipeType;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraftforge.common.ForgeHooks;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.Constants;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.energy.CapabilityEnergy;
import net.minecraftforge.energy.IEnergyStorage;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.ItemStackHandler;

public class GeneratorBE extends BlockEntity {

	
	// Instantiate BlockEntity
	public GeneratorBE(BlockPos pos, BlockState state) { 
		super(Registration.GENERATOR_BE.get(), pos, state); 
	}

	private final ItemStackHandler itemHandler = createHandler();
	private final CustomEnergyStorage energyStorage = createEnergy();
	private final LazyOptional<IItemHandler> handler = LazyOptional.of((() -> itemHandler));
	private final LazyOptional<IEnergyStorage> energy = LazyOptional.of((() -> energyStorage));
	private int counter;
	
	
	@Override
	public void setRemoved() {
		super.setRemoved();
		handler.invalidate();
		energy.invalidate();
	}
	
	
	//
	public void tickServer(BlockState state) {
		BlockState blockState = level.getBlockState(worldPosition);
		
		if (counter > 0) {
			counter--;
			energyStorage.addEnergy(50);
			setChanged();
		}
		
		if (counter <= 0) {
			ItemStack stack = itemHandler.getStackInSlot(0);
			int burnTime = ForgeHooks.getBurnTime(stack,  RecipeType.SMELTING);
			if (burnTime > 0) {
				itemHandler.extractItem(0,  1,  false);
				counter = burnTime;
				setChanged();
			}
		}
		
		if (blockState.getValue(BlockStateProperties.POWERED) != counter > 0) {
			level.setBlock(worldPosition, blockState.setValue(BlockStateProperties.POWERED, counter > 0), Constants.BlockFlags.NOTIFY_NEIGHBORS + Constants.BlockFlags.BLOCK_UPDATE);
		}
		
		sendOutPower();
	}
	
	
	// 
	private void sendOutPower() {
		AtomicInteger capacity = new AtomicInteger(energyStorage.getEnergyStored());
		if (capacity.get() > 0) {
			for (Direction direction: Direction.values()) {
				BlockEntity blockEntity = level.getBlockEntity(worldPosition.relative(direction));
				if (blockEntity != null) {
					boolean doContinue = blockEntity.getCapability(CapabilityEnergy.ENERGY, direction).map(handler -> {
						if (handler.canReceive()) {
							int received = handler.receiveEnergy(Math.min(capacity.get(), 1000), false);
							capacity.addAndGet(-received);
							energyStorage.consumeEnergy(received);
							setChanged();
							return capacity.get() > 0;
						} else {
							return true;
						}
					}
				).orElse(true);
					if (!doContinue) {
						return;
					}
				}
			}
		}
	}
	
	
	// Load CompoundTag to Variables
	@Override
	public void load(CompoundTag tag) {
		itemHandler.deserializeNBT(tag.getCompound("inv"));
		energyStorage.deserializeNBT(tag.get("energy"));
		counter = tag.getInt("gounter");
		super.load(tag);
	}
	
	
	// Save Variables to CompoundTag 
	@Override
	public CompoundTag save(CompoundTag tag) {
		tag.put("inv", itemHandler.serializeNBT());
		tag.put("energy", energyStorage.serializeNBT());
		tag.putInt("counter", counter);
		return super.save(tag);
	}
	
	
	// 
	private ItemStackHandler createHandler() {
		return new ItemStackHandler(1) {
			
			@Override
			protected void onContentsChanged(int slot) {
				setChanged();
			}
			
			@Override
			public boolean isItemValid(int slot, @Nonnull ItemStack stack) {
				return ForgeHooks.getBurnTime(stack, RecipeType.SMELTING) > 0;
			}
			
			@Nonnull
			@Override
			public ItemStack insertItem(int slot, @Nonnull ItemStack stack, boolean simulate) {
				if (ForgeHooks.getBurnTime(stack, RecipeType.SMELTING) <= 0) {
					return stack;
				}
				return super.insertItem(slot,  stack, simulate);
			}
			
		};
	}
	
	
	private CustomEnergyStorage createEnergy( ) {
		return new CustomEnergyStorage(100000, 0) {
			
			@Override
			protected void onEnergyChanged() {
				setChanged(); 
			}
		};
	}
	
	
	@Nonnull
	@Override
	public <T> LazyOptional<T> getCapability(@Nonnull Capability<T> cap, @Nullable Direction side) {
		if (cap == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY) {
			return handler.cast();
		}
		if (cap == CapabilityEnergy.ENERGY) {
			return energy.cast();
		}
		return super.getCapability(cap, side);
	}
	

}

