package com.draqon.blocks;

import com.draqon.setup.Registration;
import com.draqon.tools.CustomEnergyStorage;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.Connection;
import net.minecraft.network.protocol.game.ClientboundBlockEntityDataPacket;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;

import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.Constants;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.energy.CapabilityEnergy;
import net.minecraftforge.energy.IEnergyStorage;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public class ConsumerBE extends BlockEntity {

    // Instantiate BlockEntity
    public ConsumerBE(BlockPos pos, BlockState state) {
        super(Registration.CONSUMER_BE.get(), pos, state);
    }

    public static final int USAGE = 10;
    private final CustomEnergyStorage energyStorage = createEnergy();
    private final LazyOptional<IEnergyStorage> energy = LazyOptional.of(() -> energyStorage);
    private boolean hasPower = false;

    
    
    

    @Override
    public void setRemoved() {
        super.setRemoved();
        energy.invalidate();
    }
    

    @Nullable
    @Override
    public ClientboundBlockEntityDataPacket getUpdatePacket() {
        CompoundTag tag = new CompoundTag();
        tag.putBoolean("hasPower", hasPower);
        return new ClientboundBlockEntityDataPacket(worldPosition, 1, tag);
    }
    
    
    // Get CompoundTag and store its sub-Tags to variables
    @Override
    public void onDataPacket(Connection net, ClientboundBlockEntityDataPacket pkt) {
        CompoundTag tag = pkt.getTag();
        hasPower = tag.getBoolean("hasPower");
    }
    
    
    // Get CompoundTag and store its sub-Tags to variables
    @Override
    public CompoundTag getUpdateTag() {
        CompoundTag tag = super.getUpdateTag();
        tag.putBoolean("hasPower", hasEnoughPowerToWork());
        return tag;
    }
    

    // Get CompoundTag and store its sub-Tags to variables
    @Override
    public void handleUpdateTag(CompoundTag tag) {
        super.handleUpdateTag(tag);
        hasPower = tag.getBoolean("hasPower");
    }
    

    public void tickServer(BlockState state) {
    	BlockState blockState = level.getBlockState(worldPosition);
        if (hasEnoughPowerToWork()) {
            energyStorage.consumeEnergy(USAGE);
    		level.setBlock(worldPosition, blockState.setValue(BlockStateProperties.POWERED, true), Constants.BlockFlags.NOTIFY_NEIGHBORS + Constants.BlockFlags.BLOCK_UPDATE);
        } else {
    		level.setBlock(worldPosition, blockState.setValue(BlockStateProperties.POWERED, false), Constants.BlockFlags.NOTIFY_NEIGHBORS + Constants.BlockFlags.BLOCK_UPDATE);
        }
    }
    

    // Add Particles
    public void tickClient(BlockState state) {
        if (hasPower) {
            BlockPos blockPos = this.worldPosition;
            level.addParticle(ParticleTypes.CLOUD, blockPos.getX()+.5, blockPos.getY() + 1.0, blockPos.getZ()+.5, 0.0, 0.0, 0.0);
        }
    }
    

    private boolean hasEnoughPowerToWork() {
        return energyStorage.getEnergyStored() >= USAGE;
    }


    @Override
    public void load(CompoundTag tag) {
        if (tag.contains("energy")) {
            energyStorage.deserializeNBT(tag.get("energy"));
        }
        super.load(tag);
    }
    

    @Override
    public CompoundTag save(CompoundTag tag) {
        tag.put("energy", energyStorage.serializeNBT());
        return super.save(tag);
    }
    
    
    // Blocks Energy Storage
    private CustomEnergyStorage createEnergy() {
        return new CustomEnergyStorage(1000, 20) {
            @Override
            protected void onEnergyChanged() {
                boolean newHasPower = hasEnoughPowerToWork();
                if (newHasPower != hasPower) {
                    hasPower = newHasPower;
                    level.sendBlockUpdated(worldPosition, getBlockState(), getBlockState(), Constants.BlockFlags.BLOCK_UPDATE);
                }
                setChanged();
            }
        };
    }
    
    
    // getCapability
    @Nonnull
    @Override
    public <T> LazyOptional<T> getCapability(@Nonnull Capability<T> cap, @Nullable Direction side) {
        if (cap == CapabilityEnergy.ENERGY) {
            return energy.cast();
        }
        return super.getCapability(cap, side);
    }
    
    
}