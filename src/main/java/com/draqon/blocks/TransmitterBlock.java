package com.draqon.blocks;

import java.util.List;
import javax.annotation.Nullable;

import net.minecraft.core.BlockPos;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.EntityBlock;
import net.minecraft.world.level.block.SoundType;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityTicker;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraft.world.level.material.Material;


public class TransmitterBlock extends Block implements EntityBlock {

	
	// Instantiate Block
    public TransmitterBlock() {
        super(Properties.of(Material.METAL)
        		.sound(SoundType.METAL)
        		.noOcclusion()
        		.strength(2.0f));
    }
    
    
    // Add Hover Menu Text
    @Override
    public void appendHoverText(ItemStack stack, @Nullable BlockGetter reader, List<Component> list, TooltipFlag flags) {
        list.add(new TranslatableComponent("message.transmitter.tooltip"));
    }
    
    
    // Get BlockEntity
    @Nullable
    @Override
    public BlockEntity newBlockEntity(BlockPos pos, BlockState state) {
        return new TransmitterBE(pos, state);
    }
    
    
    // Define BlockStates
    @Override
	protected void createBlockStateDefinition(StateDefinition.Builder<Block, BlockState> builder) {
		builder.add(BlockStateProperties.FACING, BlockStateProperties.POWERED);
	}
    
	
    // Update BlockState FACING
	@Nullable
	@Override
	public BlockState getStateForPlacement(BlockPlaceContext context) {
		return defaultBlockState().setValue(BlockStateProperties.FACING, context.getNearestLookingDirection().getOpposite());
	}

    
	// Get BlockEntity Ticker
    @Nullable
    @Override
    public <T extends BlockEntity> BlockEntityTicker<T> getTicker(Level level, BlockState state, BlockEntityType<T> type) {
        if (level.isClientSide()) {
            return (level1, pos, state1, tile) -> {
                if (tile instanceof TransmitterBE transmitter) {
                	transmitter.tickClient(state1);
                }
            };
        } else {
            return (level1, pos, state1, tile) -> {
                if (tile instanceof TransmitterBE transmitter) {
                	transmitter.tickServer(state1);
                }
            };
        }
    }
}