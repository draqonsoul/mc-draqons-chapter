package com.draqon.datagen;

import com.draqon.draqonschapter.DraqonsChapter;
import com.draqon.setup.Registration;

import net.minecraft.data.DataGenerator;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.level.block.Blocks;
import net.minecraftforge.client.model.generators.ItemModelBuilder;
import net.minecraftforge.client.model.generators.ItemModelProvider;
import net.minecraftforge.common.data.ExistingFileHelper;
import static com.draqon.setup.ClientSetup.PICKAXE_CHARGES;

public class Items extends ItemModelProvider {
	
	public Items(DataGenerator generator, ExistingFileHelper existingFileHelper) {
		super(generator, DraqonsChapter.MODID, existingFileHelper);
	}
	
	@Override
	protected void registerModels() {
		
		singleTexture(
			    Registration.DRAGONEGG_COMMON.get().getRegistryName().getPath(), 
			    new ResourceLocation("item/handheld"),
			    "layer0",
			    new ResourceLocation(DraqonsChapter.MODID, "item/dragonegg0")
		);
		singleTexture(
			    Registration.DRAGONEGG_WATER.get().getRegistryName().getPath(), 
			    new ResourceLocation("item/handheld"),
			    "layer0",
			    new ResourceLocation(DraqonsChapter.MODID, "item/dragonegg1")
		);
		singleTexture(
			    Registration.DRAGONEGG_FIRE.get().getRegistryName().getPath(), 
			    new ResourceLocation("item/handheld"),
			    "layer0",
			    new ResourceLocation(DraqonsChapter.MODID, "item/dragonegg2")
		);
		singleTexture(
			    Registration.DRAGONEGG_VOID.get().getRegistryName().getPath(), 
			    new ResourceLocation("item/handheld"),
			    "layer0",
			    new ResourceLocation(DraqonsChapter.MODID, "item/dragonegg3")
		);
		singleTexture(
			    Registration.DRAGONEGG_GOLD.get().getRegistryName().getPath(), 
			    new ResourceLocation("item/handheld"),
			    "layer0",
			    new ResourceLocation(DraqonsChapter.MODID, "item/dragonegg4")
		);
		singleTexture(
			    Registration.WINGS.get().getRegistryName().getPath(), 
			    new ResourceLocation("item/handheld"),
			    "layer0",
			    new ResourceLocation(DraqonsChapter.MODID, "item/wings")
		);

		
		getBuilder(Registration.CHARGEPICKAXE.get().getRegistryName().getPath())
			.parent(getExistingFile(mcLoc("item/handheld")))
			.texture("layer0", "item/chargepickaxe0")
			.override().predicate(PICKAXE_CHARGES, 0).model(createTestModel(0)).end()
			.override().predicate(PICKAXE_CHARGES, 1).model(createTestModel(1)).end()
			.override().predicate(PICKAXE_CHARGES, 2).model(createTestModel(2)).end()
			.override().predicate(PICKAXE_CHARGES, 3).model(createTestModel(3)).end();
		
		withExistingParent(Registration.GENERATOR_ITEM.get().getRegistryName().getPath(), new ResourceLocation(DraqonsChapter.MODID, "block/generator"));
		withExistingParent(Registration.CONSUMER_ITEM.get().getRegistryName().getPath(), new ResourceLocation(DraqonsChapter.MODID, "block/consumer"));	
		withExistingParent(Registration.TRANSMITTER_ITEM.get().getRegistryName().getPath(), new ResourceLocation(DraqonsChapter.MODID, "block/transmitter"));	
	};
	
	private ItemModelBuilder createTestModel(int suffix) {
		return getBuilder("chargepickaxe" + suffix).parent(getExistingFile(mcLoc("item/handheld")))
			.texture("layer0", "item/chargepickaxe" + suffix);
	}
}
