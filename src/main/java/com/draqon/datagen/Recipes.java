package com.draqon.datagen;

import com.draqon.setup.Registration;
import net.minecraft.advancements.critereon.InventoryChangeTrigger;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.recipes.FinishedRecipe;
import net.minecraft.data.recipes.RecipeProvider;
import net.minecraft.data.recipes.ShapedRecipeBuilder;
import net.minecraft.world.item.Items;
import java.util.function.Consumer;

public class Recipes extends RecipeProvider{

	public Recipes(DataGenerator generatorIn) {
		super(generatorIn);
	}
	
	@Override
	protected void buildCraftingRecipes(Consumer<FinishedRecipe> consumer) {
		ShapedRecipeBuilder.shaped(Registration.CHARGEPICKAXE.get())
			.pattern(" x ")
			.pattern("   ")
			.pattern("   ")
			.define('x', Items.STICK)
			.group("draqonschapter")
			.unlockedBy("sticks", InventoryChangeTrigger.TriggerInstance.hasItems(Items.STICK))
			.save(consumer);

		ShapedRecipeBuilder.shaped(Registration.GENERATOR.get())
			.pattern(" x ")
			.pattern(" x ")
			.pattern("   ")
			.define('x', Items.STICK)
			.group("draqonschapter")
			.unlockedBy("sticks", InventoryChangeTrigger.TriggerInstance.hasItems(Items.STICK))
			.save(consumer);
		
		ShapedRecipeBuilder.shaped(Registration.CONSUMER.get())
			.pattern(" x ")
			.pattern(" x ")
			.pattern(" x ")
			.define('x', Items.STICK)
			.group("draqonschapter")
			.unlockedBy("sticks", InventoryChangeTrigger.TriggerInstance.hasItems(Items.STICK))
			.save(consumer);
		
		ShapedRecipeBuilder.shaped(Registration.TRANSMITTER.get())
			.pattern(" x ")
			.pattern(" x ")
			.pattern("xx ")
			.define('x', Items.STICK)
			.group("draqonschapter")
			.unlockedBy("sticks", InventoryChangeTrigger.TriggerInstance.hasItems(Items.STICK))
			.save(consumer);
		
	}
}
