package com.draqon.datagen;


import com.draqon.draqonschapter.DraqonsChapter;
import com.draqon.setup.Registration;
import com.google.common.base.Function;

import net.minecraft.core.Direction;
import net.minecraft.data.DataGenerator;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraftforge.client.model.generators.BlockModelBuilder;
import net.minecraftforge.client.model.generators.BlockStateProvider;
import net.minecraftforge.client.model.generators.ModelFile;
import net.minecraftforge.common.data.ExistingFileHelper;
import net.minecraftforge.client.model.generators.ConfiguredModel;

public class BlockStates extends BlockStateProvider {
	
	public BlockStates(DataGenerator gen, ExistingFileHelper exFileHelper) {
		super(gen, DraqonsChapter.MODID, exFileHelper);
	}

	@Override
	protected void registerStatesAndModels() {
		registerGeneratorBlock();
		registerConsumerBlock();
		registerTransmitterBlock();
    }

    @SuppressWarnings("unused")
	private void registerConsumerBlock() {
		ResourceLocation up = new ResourceLocation(DraqonsChapter.MODID, "block/metal_tile_5");
		ResourceLocation down = new ResourceLocation(DraqonsChapter.MODID, "block/metal_tile_14");
		ResourceLocation north = new ResourceLocation(DraqonsChapter.MODID, "block/metal_tile_11");
		ResourceLocation east = new ResourceLocation(DraqonsChapter.MODID, "block/metal_tile_1");
		ResourceLocation south = new ResourceLocation(DraqonsChapter.MODID, "block/metal_tile_2");
		ResourceLocation west = new ResourceLocation(DraqonsChapter.MODID, "block/metal_tile_1");
		ResourceLocation up_powered = new ResourceLocation(DraqonsChapter.MODID, "block/metal_tile_5_powered");
		ResourceLocation down_powered = new ResourceLocation(DraqonsChapter.MODID, "block/metal_tile_14_powered");
		ResourceLocation north_powered = new ResourceLocation(DraqonsChapter.MODID, "block/metal_tile_11_powered");
		ResourceLocation east_powered = new ResourceLocation(DraqonsChapter.MODID, "block/metal_tile_1_powered");
		ResourceLocation south_powered = new ResourceLocation(DraqonsChapter.MODID, "block/metal_tile_2_powered");
		ResourceLocation west_powered = new ResourceLocation(DraqonsChapter.MODID, "block/metal_tile_1_powered");
		BlockModelBuilder modelFirstblockB = models().cube("consumer", down, up, north, south, east, west);
		BlockModelBuilder modelFirstblockPoweredB = models().cube("consumer_powered", down_powered, up_powered, north_powered, south_powered, east_powered, west_powered);

		orientedBlock(Registration.CONSUMER.get(), state -> {
			if (state.getValue(BlockStateProperties.POWERED)) {
				return modelFirstblockPoweredB;
			} else {
				return modelFirstblockB;
			}
		});
	}
    
    @SuppressWarnings("unused")
	private void registerTransmitterBlock() {
		ResourceLocation up = new ResourceLocation(DraqonsChapter.MODID, "block/metal_tile_7");
		ResourceLocation down = new ResourceLocation(DraqonsChapter.MODID, "block/metal_tile_14");
		ResourceLocation north = new ResourceLocation(DraqonsChapter.MODID, "block/metal_tile_4");
		ResourceLocation east = new ResourceLocation(DraqonsChapter.MODID, "block/metal_tile_4");
		ResourceLocation south = new ResourceLocation(DraqonsChapter.MODID, "block/metal_tile_4");
		ResourceLocation west = new ResourceLocation(DraqonsChapter.MODID, "block/metal_tile_4");
		ResourceLocation up_powered = new ResourceLocation(DraqonsChapter.MODID, "block/metal_tile_7_powered");
		ResourceLocation down_powered = new ResourceLocation(DraqonsChapter.MODID, "block/metal_tile_14_powered");
		ResourceLocation north_powered = new ResourceLocation(DraqonsChapter.MODID, "block/metal_tile_4_powered");
		ResourceLocation east_powered = new ResourceLocation(DraqonsChapter.MODID, "block/metal_tile_4_powered");
		ResourceLocation south_powered = new ResourceLocation(DraqonsChapter.MODID, "block/metal_tile_4_powered");
		ResourceLocation west_powered = new ResourceLocation(DraqonsChapter.MODID, "block/metal_tile_4_powered");
		BlockModelBuilder modelFirstblockC = models().cube("transmitter", down, up, north, south, east, west);
		BlockModelBuilder modelFirstblockPoweredC = models().cube("transmitter_powered", down_powered, up_powered, north_powered, south_powered, east_powered, west_powered);

		orientedBlock(Registration.TRANSMITTER.get(), state -> {
			if (state.getValue(BlockStateProperties.POWERED)) {
				return modelFirstblockPoweredC;
			} else {
				return modelFirstblockC;
			}
		});
	}
	
	@SuppressWarnings("unused")
	private void registerGeneratorBlock() {
		ResourceLocation up = new ResourceLocation(DraqonsChapter.MODID, "block/metal_tile_3");
		ResourceLocation down = new ResourceLocation(DraqonsChapter.MODID, "block/metal_tile_14");
		ResourceLocation north = new ResourceLocation(DraqonsChapter.MODID, "block/metal_tile_9");
		ResourceLocation east = new ResourceLocation(DraqonsChapter.MODID, "block/metal_tile_1");
		ResourceLocation south = new ResourceLocation(DraqonsChapter.MODID, "block/metal_tile_2");
		ResourceLocation west = new ResourceLocation(DraqonsChapter.MODID, "block/metal_tile_1");
		ResourceLocation up_powered = new ResourceLocation(DraqonsChapter.MODID, "block/metal_tile_3_powered");
		ResourceLocation down_powered = new ResourceLocation(DraqonsChapter.MODID, "block/metal_tile_14_powered");
		ResourceLocation north_powered = new ResourceLocation(DraqonsChapter.MODID, "block/metal_tile_9_powered");
		ResourceLocation east_powered = new ResourceLocation(DraqonsChapter.MODID, "block/metal_tile_1_powered");
		ResourceLocation south_powered = new ResourceLocation(DraqonsChapter.MODID, "block/metal_tile_2_powered");
		ResourceLocation west_powered = new ResourceLocation(DraqonsChapter.MODID, "block/metal_tile_1_powered");
		BlockModelBuilder modelFirstblock = models().cube("generator", down, up, north, south, east, west);
		BlockModelBuilder modelFirstblockPowered = models().cube("generator_powered", down_powered, up_powered, north_powered, south_powered, east_powered, west_powered);
		orientedBlock(Registration.GENERATOR.get(), state -> {
			if (state.getValue(BlockStateProperties.POWERED)) {
				return modelFirstblockPowered;
			} else {
				return modelFirstblock;
			}
		});
	}
	
	private void orientedBlock(Block block, Function<BlockState, ModelFile> modelFunc) {
		getVariantBuilder(block)
			.forAllStates(state -> {
				Direction dir = state.getValue(BlockStateProperties.FACING);
				return ConfiguredModel.builder()
						.modelFile(modelFunc.apply(state))
						.rotationX(dir.getAxis() == Direction.Axis.Y ? dir.getAxisDirection().getStep() * -90 : 0)
						.rotationY(dir.getAxis() != Direction.Axis.Y ? ((dir.get2DDataValue() + 2) % 4) * 90 : 0)
						.build();
			});
	}
	
	
}
