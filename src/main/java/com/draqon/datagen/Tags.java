package com.draqon.datagen;

import com.draqon.draqonschapter.DraqonsChapter;
import com.draqon.setup.Registration;

import net.minecraft.data.DataGenerator;
import net.minecraft.data.tags.BlockTagsProvider;
import net.minecraft.tags.BlockTags;
import net.minecraftforge.common.data.ExistingFileHelper;

public class Tags extends BlockTagsProvider {
	
	public Tags(DataGenerator generator, ExistingFileHelper helper) { 
		super(generator, DraqonsChapter.MODID, helper); 
	}
	
	@Override
	protected void addTags()  {
		tag(BlockTags.MINEABLE_WITH_PICKAXE)
			.add(Registration.GENERATOR.get())
			.add(Registration.CONSUMER.get())
			.add(Registration.TRANSMITTER.get());
		tag(BlockTags.NEEDS_IRON_TOOL)
			.add(Registration.GENERATOR.get())
			.add(Registration.CONSUMER.get())
			.add(Registration.TRANSMITTER.get());
	}
	
	@Override
	public String getName() { 
		return "Tutorial Tags"; 
	}
}
