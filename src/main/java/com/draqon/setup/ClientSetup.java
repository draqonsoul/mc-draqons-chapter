package com.draqon.setup;

import com.draqon.blocks.GeneratorScreen;
import com.draqon.draqonschapter.DraqonsChapter;
import com.draqon.items.ChargePickaxe;
import com.draqon.items.DragonEggCommon;

import net.minecraft.client.gui.screens.MenuScreens;
import net.minecraft.client.renderer.item.ItemProperties;
import net.minecraft.resources.ResourceLocation;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraft.client.renderer.ItemBlockRenderTypes;
import net.minecraft.client.renderer.RenderType;

public class ClientSetup {
	
	public static final ResourceLocation PICKAXE_CHARGES = new ResourceLocation(DraqonsChapter.MODID, "charges");

	
	public static void setup(final FMLClientSetupEvent event) {
		event.enqueueWork(() -> {
			MenuScreens.register(Registration.GENERATOR_CONTAINER.get(), GeneratorScreen::new);
			initChargePickaxeOverrides();
		});
	}
	
	public static void initChargePickaxeOverrides() {
		ChargePickaxe chargepickaxe = Registration.CHARGEPICKAXE.get();
		ItemProperties.register(chargepickaxe,  PICKAXE_CHARGES, 
			(stack, level, entity, damage) -> chargepickaxe.getDistance(stack));
		
	}
}
