package com.draqon.setup;

import static com.draqon.draqonschapter.DraqonsChapter.MODID;

import com.draqon.blocks.ConsumerBE;
import com.draqon.blocks.ConsumerBlock;
import com.draqon.blocks.GeneratorBE;
import com.draqon.blocks.GeneratorBlock;
import com.draqon.blocks.GeneratorContainer;
import com.draqon.blocks.TransmitterBE;
import com.draqon.blocks.TransmitterBlock;
import com.draqon.items.ChargePickaxe;
import com.draqon.items.DragonEggCommon;
import com.draqon.items.DragonEggFire;
import com.draqon.items.DragonEggGold;
import com.draqon.items.DragonEggVoid;
import com.draqon.items.DragonEggWater;
import com.draqon.items.Wings;

import net.minecraft.core.BlockPos;
import net.minecraft.world.inventory.MenuType;
import net.minecraft.world.item.BlockItem;
import net.minecraft.world.item.CreativeModeTab;
import net.minecraft.world.item.Item;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraftforge.common.extensions.IForgeContainerType;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.minecraftforge.fmllegacy.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;

public class Registration {
	
	private static final DeferredRegister<Item> ITEMS = DeferredRegister.create(ForgeRegistries.ITEMS, MODID);
	private static final DeferredRegister<Block> BLOCKS = DeferredRegister.create(ForgeRegistries.BLOCKS, MODID);
	private static final DeferredRegister<BlockEntityType<?>> BLOCKENTITIES = DeferredRegister.create(ForgeRegistries.BLOCK_ENTITIES, MODID);
	private static final DeferredRegister<MenuType<?>> CONTAINERS = DeferredRegister.create(ForgeRegistries.CONTAINERS, MODID);
	
	public static void init() {
		IEventBus bus = FMLJavaModLoadingContext.get().getModEventBus();
		ITEMS.register(bus);
		BLOCKS.register(bus);
		BLOCKENTITIES.register(bus);
		CONTAINERS.register(bus);
	}
	
    public static final RegistryObject<ChargePickaxe> CHARGEPICKAXE = ITEMS.register("chargepickaxe", () -> new ChargePickaxe(new Item.Properties().tab(CreativeModeTab.TAB_TOOLS)));
    public static final RegistryObject<DragonEggCommon> DRAGONEGG_COMMON = ITEMS.register("dragoneggcommon",() -> new DragonEggCommon(new Item.Properties().tab(CreativeModeTab.TAB_MISC)));
    public static final RegistryObject<DragonEggWater> DRAGONEGG_WATER = ITEMS.register("dragoneggwater",() -> new DragonEggWater(new Item.Properties().tab(CreativeModeTab.TAB_MISC)));
    public static final RegistryObject<DragonEggFire> DRAGONEGG_FIRE = ITEMS.register("dragoneggfire",() -> new DragonEggFire(new Item.Properties().tab(CreativeModeTab.TAB_MISC)));
    public static final RegistryObject<DragonEggVoid> DRAGONEGG_VOID = ITEMS.register("dragoneggvoid",() -> new DragonEggVoid(new Item.Properties().tab(CreativeModeTab.TAB_MISC)));
    public static final RegistryObject<DragonEggGold> DRAGONEGG_GOLD = ITEMS.register("dragonegggold",() -> new DragonEggGold(new Item.Properties().tab(CreativeModeTab.TAB_MISC)));
    public static final RegistryObject<Wings> WINGS = ITEMS.register("wings",() -> new Wings(new Item.Properties().tab(CreativeModeTab.TAB_MISC)));
    
    public static final RegistryObject<GeneratorBlock> GENERATOR = BLOCKS.register("generator", GeneratorBlock::new);
    public static final RegistryObject<Item> GENERATOR_ITEM = ITEMS.register("generator", () -> new BlockItem(GENERATOR.get(), new Item.Properties().tab(CreativeModeTab.TAB_MISC)));
    public static final RegistryObject<BlockEntityType<GeneratorBE>> GENERATOR_BE = BLOCKENTITIES.register("generator", 
    		() -> BlockEntityType.Builder.of(GeneratorBE::new, GENERATOR.get()).build(null));
    
    public static final RegistryObject<MenuType<GeneratorContainer>> GENERATOR_CONTAINER = CONTAINERS.register("generator", () -> IForgeContainerType.create((windowId, inv, data) -> {
    		BlockPos pos = data.readBlockPos();
    		Level world = inv.player.getCommandSenderWorld();
    		return new GeneratorContainer(windowId, world, pos, inv, inv.player);
	}));
    
    public static final RegistryObject<ConsumerBlock> CONSUMER = BLOCKS.register("consumer", ConsumerBlock::new);
    public static final RegistryObject<Item> CONSUMER_ITEM = ITEMS.register("consumer", () -> new BlockItem(CONSUMER.get(), new Item.Properties().tab(CreativeModeTab.TAB_MISC)));
    public static final RegistryObject<BlockEntityType<ConsumerBE>> CONSUMER_BE = BLOCKENTITIES.register("consumer", 
    		() -> BlockEntityType.Builder.of(ConsumerBE::new, CONSUMER.get()).build(null));
        
    public static final RegistryObject<TransmitterBlock> TRANSMITTER = BLOCKS.register("transmitter", TransmitterBlock::new);
    public static final RegistryObject<Item> TRANSMITTER_ITEM = ITEMS.register("transmitter", () -> new BlockItem(TRANSMITTER.get(), new Item.Properties().tab(CreativeModeTab.TAB_MISC)));
    public static final RegistryObject<BlockEntityType<TransmitterBE>> TRANSMITTER_BE = BLOCKENTITIES.register("transmitter", 
    		() -> BlockEntityType.Builder.of(TransmitterBE::new, TRANSMITTER.get()).build(null));
    
    
    
    
    
    
    
}
