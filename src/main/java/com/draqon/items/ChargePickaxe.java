package com.draqon.items;

import java.util.List;

import javax.annotation.Nullable;

import net.minecraft.ChatFormatting;
import net.minecraft.Util;
import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResultHolder;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.PickaxeItem;
import net.minecraft.world.item.Tiers;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.ClipContext;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.Vec3;
import net.minecraft.world.phys.HitResult;

public class ChargePickaxe extends PickaxeItem {
	
	// Construct Item
	public ChargePickaxe(Properties properties) {
		super(Tiers.NETHERITE, 1, -2.8F, properties);
	}
	
	
	// Add Hover Menu Text
	@Override
	public void appendHoverText(ItemStack stack, @Nullable Level level, List<Component> list, TooltipFlag flags) {
		super.appendHoverText(stack, level, list, flags);
		int charges = stack.hasTag() ? stack.getTag().getInt("charges") : 0;
		list.add(new TranslatableComponent("message.chargepickaxe.tooltip", Integer.toString(charges)).withStyle(ChatFormatting.GOLD));
	}
	
	@Override
	public InteractionResultHolder<ItemStack> use(Level level, Player player, InteractionHand hand) {
		ItemStack stack = player.getItemInHand(hand);
		int charges = stack.getOrCreateTag().getInt("charges");
		charges++;
		if(charges>3) {
			charges = 3;
		}
		stack.getTag().putInt("charges", charges);
		if(level.isClientSide()) {
			player.sendMessage(new TranslatableComponent("message.chargepickaxe.change", Integer.toString(charges)), Util.NIL_UUID);
		}
		return InteractionResultHolder.success(stack);
	}
	
	public int getDistance(ItemStack stack) { return stack.hasTag() ? stack.getTag().getInt("charges") : 0; }
	
	@Override
	public boolean mineBlock(ItemStack stack, Level level, BlockState state, BlockPos pos, LivingEntity livingEntity) {
		boolean result = super.mineBlock(stack, level, state, pos, livingEntity);
		if (result) {
			int distance = getDistance(stack);
			if (distance > 0) {
				CompoundTag tag = stack.getOrCreateTag();
				boolean mining = tag.getBoolean("mining");
				if (!mining) {
					BlockHitResult hit = trace(level, livingEntity);
					if (hit.getType() == HitResult.Type.BLOCK) {
						tag.putBoolean("mining", true);
						for (int i = 0; i < distance; i++) {
							BlockPos relative = pos.relative(hit.getDirection().getOpposite(), i + 1);
							if (!tryHarvest(stack, livingEntity, relative)) {
								tag.putBoolean("mining", false);
								return result;
							}
						}
						tag.putBoolean("mining", false);
					}
				}
			}
		}
		return result;
	}
	
	private boolean tryHarvest(ItemStack stack, LivingEntity livingEntity, BlockPos pos) {
		BlockState state = livingEntity.level.getBlockState(pos);
		if (isCorrectToolForDrops(stack, state)) {
			if (livingEntity instanceof ServerPlayer player) {
				return player.gameMode.destroyBlock(pos);
			}
		}
		return false;
	}
	
	private BlockHitResult trace(Level level, LivingEntity player) {
		double reach = player.getAttribute(net.minecraftforge.common.ForgeMod.REACH_DISTANCE.get()).getValue();
		Vec3 eye = player.getEyePosition(1.0f);
		Vec3 view = player.getViewVector(1.0f);
		Vec3 withReach = eye.add(view.x + reach, view.y * reach, view.z * reach);
		return level.clip(new ClipContext(eye, withReach, ClipContext.Block.OUTLINE, ClipContext.Fluid.NONE, player));
	}
	
}
