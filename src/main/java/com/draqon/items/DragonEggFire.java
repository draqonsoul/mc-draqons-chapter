package com.draqon.items;

import java.util.List;
import javax.annotation.Nullable;

import net.minecraft.ChatFormatting;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;

public class DragonEggFire extends Item {
		
	// Construct Item
	public DragonEggFire(Properties properties) {
		super(properties);
	}
	
	
	// Add Hover Menu Text
	@Override
	public void appendHoverText(ItemStack stack, @Nullable Level level, List<Component> list, TooltipFlag flags) {
		super.appendHoverText(stack, level, list, flags);
		list.add(new TranslatableComponent("message.dragoneggfire.tooltip", ("")).withStyle(ChatFormatting.GOLD));
	}
	
}
