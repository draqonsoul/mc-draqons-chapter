package com.draqon.draqonschapter;

import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.draqon.setup.ClientSetup;
import com.draqon.setup.Registration;

@Mod(DraqonsChapter.MODID)
public class DraqonsChapter
{
	public static final String MODID = "draqonschapter";
	
    public static final Logger LOGGER = LogManager.getLogger();

    public DraqonsChapter() {
    	Registration.init();
    	
    	IEventBus bus = FMLJavaModLoadingContext.get().getModEventBus();
    	bus.addListener(this::setup);
    	bus.addListener(ClientSetup::setup);
        MinecraftForge.EVENT_BUS.register(this);
    }

    private void setup(final FMLCommonSetupEvent event) {
        LOGGER.info("HELLO FROM PREINIT");
    }
}
